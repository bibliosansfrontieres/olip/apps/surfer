#!/bin/sh

set -eu

mkdir -p /data/

HOWTO_FILENAME='surfer-howto.mp4'
HOWTO_PATH="/data/${HOWTO_FILENAME}"
[ -f "${HOWTO_PATH}" ] || {
    echo "=> Installing the default content file (${HOWTO_FILENAME})"
    cp "/$HOWTO_FILENAME" "$HOWTO_PATH" \
        && touch --date 1970-01-01 "$HOWTO_PATH"
}

echo "=> Start the server"
exec node /app/code/server.js /data/
