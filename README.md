# Surfer

Surfer is a Simple static file server:

* Logged in users can upload files from their local folders ;
* Visitors can browse files.
* Any index.html file in a directory will be served up automatically.

The App comes with a pre-defined user account:

* login: `admin`
* password: `admin`

## descriptor.json

```json
{
    "version": "0.0.2",
    "contents": [],
    "bundle": "surfer.app",
    "containers": [
        {
            "expose": 3000,
            "name": "surfer",
            "image": "offlineinternet/surfer:latest"
        }
    ],
    "name": "surfer",
    "description": "File browsing",
    "picture": "#surfer.png"
},
```

## This image

* Project: <https://gitlab.com/bibliosansfrontieres/olip/apps/surfer>
* DockerHub: [`offlineinternet/surfer`](https://hub.docker.com/r/offlineinternet/surfer)
* GitLab Registry: [`registry.gitlab.com/bibliosansfrontieres/olip/apps/surfer/surfer`](https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/container_registry)

## Upstream

* Repository: <https://git.cloudron.io/cloudron/surfer>
* Changelog: <https://git.cloudron.io/cloudron/surfer/-/blob/master/CHANGELOG>
