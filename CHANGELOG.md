# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

...

## [0.0.5] - 2022-01-16

### App

- Provision the app with a howto ([#4](https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/issues/4))
- Replace upstream logo/favicon for a more inclusive one
  ([#3](https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/issues/3),
  [#5](https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/issues/5))
- Upgrade Surfer to v5.17.0

### OLIP packaging

- Added this Changelog
- Pin the Surfer version
- Multiple Docker image improvements
- Remove leftover files from the image

## [0.0.4] - 2021-09-31

- Switch the CICD to use our K8S cluster
- Build multiarch images

## [0.0.3] - 2021-05-19

- Use olip-base image

## [0.0.2] - 2021-01-26

- Upgrade Surfer to v5.13.5-1

## [0.0.1] - 2020-15-21

- Initial release

[Unreleased]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/compare/v0.0.5...HEAD
[0.0.5]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/compare/v0.0.4...v0.0.5
[0.0.4]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/bibliosansfrontieres/olip/apps/surfer/-/tags/v0.0.1
