# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
  case $ARCH in \
    armhf) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-armv7l.tar.gz' ;; \
    amd64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-x64.tar.gz' ;; \
    i386) node_build='https://unofficial-builds.nodejs.org/download/release/v16.4.2/node-v16.4.2-linux-x86.tar.gz' ;; \
    arm64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-arm64.tar.gz' ;; \
    *) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
  esac && \
  wget --quiet $node_build -O node.tar.gz && \
  tar -C /usr/local -xzf node.tar.gz --strip=1 && \
  rm node.tar.gz

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN set -eux; \
  ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
  if [[ "$ARCH" = "armhf" ]]; then \
    apt-get update; \
    apt-get install -y --no-install-recommends libatomic1 ; \
  fi \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/code

RUN mkdir -p /app/code \
  && wget -nv https://git.cloudron.io/cloudron/surfer/-/archive/v5.17.0/surfer-master.tar.gz -O /tmp/surfer.tar.gz \
  && tar -xvf /tmp/surfer.tar.gz --strip=1 --wildcards \
      surfer-*/src/ \
      surfer-*/frontend/ \
      surfer-*/cli/ \
      surfer-*/public/ \
      surfer-*/vue.config.js \
      surfer-*/babel.config.js \
      surfer-*/server.js \
      surfer-*/admin \
      surfer-*/package.json \
      surfer-*/package-lock.json \
  && sed -i '/chromedriver/d' package.json \
  && rm /tmp/surfer.tar.gz \
  \
  && wget -nv https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/surfer.png \
  -O /app/code/favicon.png

# hadolint ignore=DL3059
RUN npm install \
    && npm run build

COPY surfer-howto.mp4 /surfer-howto.mp4
COPY start.sh .config.json .users.json /app/code/

EXPOSE 3000
CMD [ "/app/code/start.sh" ]
